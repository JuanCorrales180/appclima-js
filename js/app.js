
const resultado = document.querySelector("#resultado");
const formulario = document.querySelector("#formulario");
const paisOption = document.querySelector("#pais");

window.addEventListener('load', () =>{
    formulario.addEventListener('submit', buscarClima)
    cargarPaises();
});

// document.addEventListener('DOMContentLoaded', cargarPaises)


function cargarPaises(){
    console.log("Cargando los countries")
    const rutaPaises = "js/paises2.json";
    fetch(rutaPaises)
        .then((respuesta) =>{
            // return respuesta.json();
            console.log(respuesta)
           return respuesta.json();
        })
        .then(paises =>{

            const arrayPaises = paises.countries
            console.log(arrayPaises)
            let html= "";

            arrayPaises.forEach((pais) =>{
                html += `<option value="${pais.code}">${pais.name_es}</option>`;
                paisOption.innerHTML = html;
            })


        }).catch(error =>{
            console.log(error)
        })

    
}

function buscarClima(e){
    e.preventDefault();
    const ciudad = document.querySelector("#ciudad").value;
    const pais = document.querySelector("#pais").value;

    if(ciudad === '' || pais === ''){
        mostrarMensaje("Los campos son obligatorios")
        return;
    }

    consultarAPI(ciudad, pais)
}

function mostrarMensaje(mensaje){
    const alertaClass = document.querySelector(".mi-alerta")
    const alerta = document.createElement('div');
    alerta.classList.add('alert', 'alert-danger','text-center','mi-alerta')
    alerta.innerHTML = mensaje   

    if(!alertaClass){
        formulario.after(alerta)

        setTimeout(() =>{
            alerta.remove();
        },5000)
    }
}

function consultarAPI(ciudad, pais){
    // https://github.com/jackocnr/intl-tel-input/blob/master/src/js/data.js 

    // https://tobiasahlin.com/spinkit/

    const appId = '096ef0039b9a6f47d47b49c3f7123f36';
    const url = `https://api.openweathermap.org/data/2.5/weather?q=${ciudad},${pais}&appid=${appId}`;

    spinner();

    fetch(url)
        .then(respuesta =>{
            return respuesta.json()
        })
        .then(datos =>{ 
            limpiarHTML();
            if(datos.cod === "404"){
                mostrarMensaje("Ciudad no encontrada")
                return;
            }
            mostrarClima(datos);
        })
}

function mostrarClima(datos){
    const {name, main:{temp, temp_max, temp_min}} = datos;
    const centigrados = kevinACentigrados(temp);
    const max = kevinACentigrados(temp_max);
    const min = kevinACentigrados(temp_min);
    
    const actual = document.createElement('p')
    actual.innerHTML = `<strong>${centigrados} &#8451;</strong>`;
    actual.classList.add('display-1')

    const nombreCuidad = document.createElement('p')
    nombreCuidad.innerHTML = `<strong>Clima en ${name}</strong>`;
    nombreCuidad.classList.add('display-6')
    resultado.appendChild(nombreCuidad);

    const resultadoDiv = document.createElement('div');
    resultadoDiv.classList.add('text-center');
    resultadoDiv.appendChild(actual);
    resultado.appendChild(resultadoDiv)

    const tempMaxima = document.createElement('p')
    tempMaxima.innerHTML = `<strong>Max: ${max} &#8451 </strong>`
    tempMaxima.classList.add('text-center');
    resultado.appendChild(tempMaxima)

    const tempMinima= document.createElement('p')
    tempMinima.innerHTML = `<strong>Min: ${min} &#8451 </strong>`
    tempMinima.classList.add('text-center');
    resultado.appendChild(tempMinima)
}

function limpiarHTML(){
    while(resultado.firstChild){
        resultado.removeChild(resultado.firstChild)
    }
}

function kevinACentigrados(temp){
    return parseInt(temp -273.15)
}

function spinner(){
    limpiarHTML();
    const divSpinner = document.createElement('div');
    divSpinner.classList.add('sk-fading-circle')
    divSpinner.innerHTML = `
        <div class="sk-circle1 sk-circle"></div>
        <div class="sk-circle2 sk-circle"></div>
        <div class="sk-circle3 sk-circle"></div>
        <div class="sk-circle4 sk-circle"></div>
        <div class="sk-circle5 sk-circle"></div>
        <div class="sk-circle6 sk-circle"></div>
        <div class="sk-circle7 sk-circle"></div>
        <div class="sk-circle8 sk-circle"></div>
        <div class="sk-circle9 sk-circle"></div>
        <div class="sk-circle10 sk-circle"></div>
        <div class="sk-circle11 sk-circle"></div>
        <div class="sk-circle12 sk-circle"></div>
    `
    resultado.appendChild(divSpinner);
}